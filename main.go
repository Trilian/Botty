package main

import (
	"log"
	"trilian-bot/bot"
	"trilian-bot/config"
)

func main() {
	err := config.ReadConfig()
	if err != nil {
		log.Fatal(err)
		return
	}
	bot.Run()
	<-make(chan struct{})
	return
}
