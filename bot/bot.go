package bot

import (
	"log"
	"strings"
	"trilian-bot/config"

	"github.com/bwmarrin/discordgo"
)

var (
	BotID string
	goBot *discordgo.Session
)

func Run() {
	goBot, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		log.Fatal(err)
		return
	}

	user, err := goBot.User("@me")
	if err != nil {
		log.Fatal(err)
		return
	}

	BotID = user.ID
	goBot.AddHandler(messageHandler)
	err = goBot.Open()
	if err != nil {
		log.Fatal(err)
		return
	}

}

func messageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == BotID {
		return
	}

	if m.Content == config.Prefix+"ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	if m.Content == config.Prefix+"help" {
		s.ChannelMessageSend(m.ChannelID, "Hilfe gibt's hier nicht!")
	}

	if strings.HasPrefix(m.Content, config.Prefix+"play ") {
		// TODO: implement youtube player
		youtubeHandler(s, m)
	}
}

func youtubeHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	videoLink := strings.TrimPrefix(m.Content, config.Prefix+"play ")
	s.ChannelMessageSend(m.ChannelID, "Wenn ich Zeit habe spiele ich ein Video ab.\nLink: "+videoLink)
}
